import React, { useEffect, useState } from "react";
import Heading from "./components/Heading";
import Edit from "./pages/Edit";
import CreateUser from "./pages/CreateUser";
import { Routes, Route } from "react-router-dom";
import { studentList, Student } from "./data";
import Layout from "./components/Layout";
import Pagination from "./components/Pagination";

import "./App.css";

function App() {
  const [listStudent, setListStudent] = useState<Array<Student>>(studentList);

  console.log("initial: ", listStudent);

  const [studentEdit, setStudentEdit] = useState<Student>({
    id: "",
    avatar: "",
    firstName: "",
    lastName: "",
    dob: "",
    gender: "",
  });

  console.log("current: ", studentEdit);

   const [currentPage, setCurrentPage] = useState(1);
   const [postsPerPage] = useState(4);

  


  const handleEdit = (id: string) => {
    const student = listStudent.find((item: Student) => {
      return id === item.id ? item : "";
    });

    console.log("find: ", student);
    if (student) setStudentEdit(student);
  };

  const handleUpdate = (student: Student): void => {
    const studentUpdate = listStudent.map((item: Student) => {
      if (student.id === item.id) {
        return student;
      }
      return item;
    });
    setListStudent([...studentUpdate]);
  };

  const handleAddUser = (student: Student) => {
    const find = listStudent.some(item => item.id === student.id);
    console.log("check: ", find);
    if(!find) setListStudent([...listStudent, student]);
   
  };

  const handleDelete = (id: string) => {
    setListStudent(listStudent.filter(item => {
      if (id != item.id){
        return item;
      }
    }))
  }

   //Get current posts
   const indexOfLastPost: number = currentPage * postsPerPage;
   const indexOfFirstPost: number = indexOfLastPost - postsPerPage;
   const currentPosts: Array<Student> = listStudent.slice(indexOfFirstPost, indexOfLastPost);



   //change page
   const paginate = (pageNumber: number) => setCurrentPage(pageNumber)


  

  return (
    <div className="App">
      <Heading />

      <Routes>
      <Route
          path="/"
          element={ <Layout listStudent={currentPosts} handleEdit={handleEdit} handleDelete={handleDelete} />}
        />
        

        <Route
          path="/edit"
          element={<Edit handle={studentEdit} handleUpdate={handleUpdate} />}
        />

        <Route
          path="/create"
          element={<CreateUser handleAddUser={handleAddUser} handle={studentEdit}/>}
        />
      </Routes>

      <Pagination postsPerPage={postsPerPage} totalPosts={listStudent.length} paginate={paginate}/>
    </div>
  );
}

export default App;
