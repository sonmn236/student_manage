import React, { FC, useState, useEffect } from "react";
import { Student } from "../data";
import { useNavigate } from "react-router-dom";

interface Props {
  handle: Student;
  handleUpdate(student: Student): void;
}

const Edit: FC<Props> = (props) => {
  const [studentEdit, setStudentEdit] = useState({
    id: "",
    avatar: "",
    firstName: "",
    lastName: "",
    dob: "",
    gender: "",
  });

  const redirect = useNavigate();

  const handleSubmit = (e: any) => {
    e.preventDefault();
    props.handleUpdate(studentEdit);
    redirect("/");
  };

  const handleChange = (e: any) => {
    var name = e.target.name;
    setStudentEdit({ ...studentEdit, [name]: e.target.value });
  };
  useEffect(() => {
    setStudentEdit(props.handle);
  }, [props.handle]);

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <table>
          <tr>
            <td>ID</td>
            <td>
              <input
                type="text"
                placeholder="input id"
                name="id"
                value={studentEdit.id}
                onChange={handleChange}
              ></input>
            </td>
          </tr>
          <tr>
            <td>Avatar</td>
            <td>
              <input
                type="text"
                placeholder="input avatar"
                name="avatar"
                value={studentEdit.avatar}
                onChange={handleChange}
              ></input>
            </td>
          </tr>
          <tr>
            <td>FirstName</td>
            <td>
              <input
                type="text"
                placeholder="input first name"
                name="firstName"
                value={studentEdit.firstName}
                onChange={handleChange}
              ></input>
            </td>
          </tr>
          <tr>
            <td>LastName</td>
            <td>
              <input
                type="text"
                placeholder="input last name"
                name="lastName"
                value={studentEdit.lastName}
                onChange={handleChange}
              ></input>
            </td>
          </tr>
          <tr>
            <td>Dob</td>
            <td>
              <input
                type="text"
                placeholder="input dob"
                name="dob"
                value={studentEdit.dob}
                onChange={handleChange}
              ></input>
            </td>
          </tr>
          <tr>
            <td>Gender</td>
            <td>
              <input
                type="text"
                placeholder="input gender"
                name="gender"
                value={studentEdit.gender}
                onChange={handleChange}
              ></input>
            </td>
          </tr>
          <tr>
            <td>
              <button type="submit">Save</button>
            </td>
          </tr>
        </table>
      </form>
    </div>
  );
};

export default Edit;
