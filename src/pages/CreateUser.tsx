import React, { FC, useState, useEffect } from "react";
import { Student } from "../data";
import { useNavigate } from "react-router-dom";

interface Props {
  handle: Student;
  handleAddUser(student: Student): void;
}

const CreateUser: FC<Props> = (props) => {
  const [newStudent, setNewStudent] = useState({
    id: "",
    avatar: "",
    firstName: "",
    lastName: "",
    dob: "",
    gender: "",
  });

  const redirect = useNavigate();

  const handleSubmit = (e: any) => {
    e.preventDefault();
    props.handleAddUser(newStudent);
    redirect("/");
  };

  const handleChange = (e: any) => {
    var name = e.target.name;
    setNewStudent({ ...newStudent, [name]: e.target.value });
  };
  

  useEffect(() => {
    setNewStudent(props.handle);
  }, [props.handle]);

  return (
    <div>
      <div>
        <form onSubmit={handleSubmit}>
          <table>
            <tr>
              <td>ID</td>
              <td>
                <input
                  type="text"
                  placeholder="input id"
                  name="id"
                  value={newStudent.id}
                  onChange={handleChange}
                ></input>
              </td>
            </tr>
            <tr>
              <td>Avatar</td>
              <td>
                <input
                  type="text"
                  placeholder="input avatar"
                  name="avatar"
                  value={newStudent.avatar}
                  onChange={handleChange}
                ></input>
              </td>
            </tr>
            <tr>
              <td>FirstName</td>
              <td>
                <input
                  type="text"
                  placeholder="input first name"
                  name="firstName"
                  value={newStudent.firstName}
                  onChange={handleChange}
                ></input>
              </td>
            </tr>
            <tr>
              <td>LastName</td>
              <td>
                <input
                  type="text"
                  placeholder="input last name"
                  name="lastName"
                  value={newStudent.lastName}
                  onChange={handleChange}
                ></input>
              </td>
            </tr>
            <tr>
              <td>Dob</td>
              <td>
                <input
                  type="text"
                  placeholder="input dob"
                  name="dob"
                  value={newStudent.dob}
                  onChange={handleChange}
                ></input>
              </td>
            </tr>
            <tr>
              <td>Gender</td>
              <td>
                <input
                  type="text"
                  placeholder="input gender"
                  name="gender"
                  value={newStudent.gender}
                  onChange={handleChange}
                ></input>
              </td>
            </tr>
            <tr>
              <td>
                <button type="submit">Create</button>
              </td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  );
};

export default CreateUser;
