

 export interface Student {
   id: string,
   avatar: string,
   firstName: string,
   lastName: string,
   dob: string,
   gender: string
}


export const studentList: Array<Student> = [
    {
        id: "1",
        avatar: "url",
        firstName: "Mai",
        lastName: "Son",
        dob: "March,31,1999",
        gender: "male"
    },
    {
        id: "2",
        avatar: "url",
        firstName: "Dinh",
        lastName: "Thuy",
        dob: "June,23,1999",
        gender: "Female"
    },
    {
        id: "3",
        avatar: "url",
        firstName: "Mai",
        lastName: "Van",
        dob: "March,05,1992",
        gender: "Female"
    },
    {
        id: "4",
        avatar: "url",
        firstName: "Mai",
        lastName: "Viet",
        dob: "May,05,1993",
        gender: "Male"
    },
    {
        id:"5",
        avatar: "url",
        firstName: "Phan",
        lastName: "Quynh",
        dob: "April,02,1993",
        gender: "Female"
    },
    {
        id: "6",
        avatar: "url",
        firstName: "Qua",
        lastName: "Qua",
        dob: "Jul,08,2021",
        gender: "Male"
    },
    {
        id: "7",
        avatar: "url",
        firstName: "Mr",
        lastName: "Adam",
        dob: "Jul,11,1999",
        gender: "Female"
    },
    {
        id: "8",
        avatar: "url",
        firstName: "Juliet",
        lastName: "Romeo",
        dob: "Jul,11,1999",
        gender: "Female"
    },
    {
        id: "9",
        avatar: "url",
        firstName: "Juliet",
        lastName: "Romeo",
        dob: "Jul,11,1999",
        gender: "Female"
    },
    {
        id: "10",
        avatar: "url",
        firstName: "Juliet",
        lastName: "Romeo",
        dob: "Jul,11,1999",
        gender: "Female"
    },
]



