import React from "react";
import './Heading.css';

function Heading() {

    const handle = () => {
        console.log('Clicked!')
    }
    return(
        <div className="head-app">
            <h3 className="title-head">Student Manage</h3>
        </div>
    )
}

export default Heading;