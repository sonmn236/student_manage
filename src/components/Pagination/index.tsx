import React, { FC } from "react";
import { Link } from "react-router-dom";
import './Pagination.css';

interface Props {
    postsPerPage: number;
    totalPosts: number;
    paginate(pageNumber: number): void
}
const Pagination: FC<Props>  = (props) => {
    const pageNumbers = [];

    for(let i =1; i <= Math.ceil(props.totalPosts / props.postsPerPage); i++){
        pageNumbers.push(i);
    }

    return (
       <nav>
           <ul className="pagination">
               {pageNumbers.map(item => (
                   <li key={item} className="page-item">
                       <Link onClick={() => props.paginate(item)} to="/"className="page-link">
                           {item}
                       </Link>
                   </li>
               ))}
           </ul>
       </nav>
    )
}

export default Pagination;


