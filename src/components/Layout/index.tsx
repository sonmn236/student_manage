import React from "react";
import { Link } from "react-router-dom";
import { Student } from "../../data";
import "./Layout.css";

interface Props {
  listStudent: Array<Student>;
  handleEdit(id: string): void;
  handleDelete(id: string): void
}

function Layout(props: Props) {
  return (
    <div className="box">
      <h4 className="title-list">Student List</h4>
      <p className="content-list">List of students in class D17</p>
      <table className="content-table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Avatar</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>DOB</th>
            <th>Gender</th>
          </tr>
        </thead>
        <tbody>
          {props.listStudent.map((student) => (
            <tr key={student.id}>
              <td>{student.id}</td>
              <td>{student.avatar}</td>
              <td>{student.firstName}</td>
              <td>{student.lastName}</td>
              <td>{student.dob}</td>
              <td>{student.gender}</td>
              <td>
                <i className="fas fa-star"></i>
              </td>
              <td>
                <i className="fas fa-eye"></i>
              </td>
              <td>
                <Link to="/edit" onClick={() => props.handleEdit(student.id)}>
                  <i className="fas fa-pen"></i>
                </Link>
              </td>
              <td onClick={() => props.handleDelete(student.id)}><i className="fas fa-trash-alt"></i></td>
            </tr>
          ))}
        </tbody>
      </table>
      <Link to="/create">
        <button className="btn-add">Add</button>
      </Link>
    </div>
  );
}

export default Layout;
